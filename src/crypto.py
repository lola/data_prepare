#!/usr/bin/env python3

from pathlib import Path
import tempfile

import cryptncompress.crypto

"""
Module used to crypt/decrypt files
"""

PUBLICKEY = b"""-----BEGIN CERTIFICATE-----
MIIDgzCCAmugAwIBAgIUdF/RfRrJcnIchKBErTkRM1xcuDEwDQYJKoZIhvcNAQEL
BQAwUTELMAkGA1UEBhMCRlIxDzANBgNVBAgMBkZyYW5jZTEOMAwGA1UEBwwFTmFu
Y3kxITAfBgNVBAoMGEludGVybmV0IFdpZGdpdHMgUHR5IEx0ZDAeFw0yMTAyMTUx
NzIyNDRaFw0yMTAzMTcxNzIyNDRaMFExCzAJBgNVBAYTAkZSMQ8wDQYDVQQIDAZG
cmFuY2UxDjAMBgNVBAcMBU5hbmN5MSEwHwYDVQQKDBhJbnRlcm5ldCBXaWRnaXRz
IFB0eSBMdGQwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCuf831hLjp
ttt5HP6cRIreXqGUT6dW+woxNNG2Y0MfxbTxWRzQB/hodjIZmHTCsK7+ZPUOO8Df
YAlhXhwG0Ke2GdPlu4KNCOVCWhpAlfakHwM8gnr1hvuH16b+dMPNAHpdvlTdp/q8
fOK2Z9yE+xP81MTc/phhcEAZxJqdYcBenNu2uMExKhhFl5mpQjGYgUwSNSODoy+S
W12gMUZ7ywOm5m4f8HoSRs4cuzVO5ooG0R3sGP7TXFaHwvPmWWZ5DgXc2e2gpREo
6Hcr3fBgYnmB+nH3xNFtfns0Juh2gF2UCjU3mUjfbcxLIsqmn+vb6nJxH+5Efr8d
fk4CmogZ+ljNAgMBAAGjUzBRMB0GA1UdDgQWBBRmM0JqrGa3EK4TV8dz2q2qnKbf
BjAfBgNVHSMEGDAWgBRmM0JqrGa3EK4TV8dz2q2qnKbfBjAPBgNVHRMBAf8EBTAD
AQH/MA0GCSqGSIb3DQEBCwUAA4IBAQAuk5wak4BsHELRG+VTKfmdE6qHZsoXmOE7
rJ9ClfFqcrs2AjMyv2L4Dav3SjpeZx8WuUvo8fPolbL2wwixOiCSYaBedYpPa1Lx
9OPrtPSUfZGMatyY3Duz2rK3cyz9tdfFD/F/s/VohXhJlgbrRgSyMKuYRceovSKF
AWRHM99+RGP34mP2bGbCqbIBdEhAN5SmhdxWEBXqGFgC0oERENKO1s0c3pP6RJYq
OXZA3dYapBH6DB+C1/q8t1w8pno3TDSsTThn+/5kT/a0ZIn7f1IFfF4ZQxJGKfSc
3uUsZzfTJYq0NAWi5k4qL71wkjXT63aQL5RucHpcpZR2yCWjQvlB
-----END CERTIFICATE-----
"""


def encrypt_file(input_file: Path, output_file: Path, file_type: str = "xapi"):
    """
    Encrypt a file using the cryptNcompress library
    """
    try:
        # Create temporary file to store the public key
        tmp_public_key_file = tempfile.NamedTemporaryFile()
        tmp_public_key_file.write(PUBLICKEY)
        tmp_public_key_file.flush()

        encryptor = cryptncompress.crypto.Encrypt(tmp_public_key_file.name)
        encryptor.encrypt_file(file_type=file_type, input_file=input_file, output_file=output_file)
    finally:
        tmp_public_key_file.close()


def decrypt_file(input_file: Path, output_file: Path, private_key: Path):
    """
    Decrypt a file using the cryptNcompress library
    """
    decryptor = cryptncompress.crypto.Decrypt(private_key)
    decryptor.decrypt_file(input_file=input_file, output_file=output_file)
