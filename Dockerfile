FROM continuumio/miniconda3
MAINTAINER Philippe Noel <philippe.noel@inria.fr>
LABEL description="Image used to build linux version of lola_toolbox"

COPY . /src

RUN apt update && \
    apt install binutils -y && \
    apt-get clean && \
    pip install -r /src/requirements.txt && \
    pip install pyinstaller

ENTRYPOINT ["pyinstaller", "/src/main.spec", "--distpath", "/tmp/dist", "--workpath", "/tmp/build"]
